#ifndef TABTEXTEDIT_H
#define TABTEXTEDIT_H

#include <QTextEdit>

class TabTextEdit : public QTextEdit {
public:
    TabTextEdit();

    bool isSaved() const;
    void setSaved(bool isSaved);

    QString getPath() const;
    void setPath(const QString &getPath);

    QString getName() const;
    void setName(const QString &getName);

    bool isNewFile() const;
    void setNewFile(bool newFile);

private:
    bool mSaved = false;
    QString mPath;
    QString mName;
};

#endif // TABTEXTEDIT_H

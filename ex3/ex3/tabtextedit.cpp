#include "tabtextedit.h"

/*
* Constructor
*/
TabTextEdit::TabTextEdit()
{}

/*
* Checks if the file has been saved yet
*/
bool TabTextEdit::isSaved() const
{
   return mSaved;
}

/*
* Sets the state of the saved member
*/
void TabTextEdit::setSaved(bool saved)
{
   mSaved = saved;
}

/*
* Returns the full path of the file 
*/
QString TabTextEdit::getPath() const
{
   return mPath;
}

/*
* Sets the full path of the file
*/
void TabTextEdit::setPath(const QString &path)
{
   mPath = path;
}

/*
* Returns the name of the file
*/
QString TabTextEdit::getName() const
{
   return mName;
}

/*
* Sets the name of the file
*/
void TabTextEdit::setName(const QString &name)
{
   mName = name;
}


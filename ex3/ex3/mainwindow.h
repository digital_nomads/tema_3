#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "tabtextedit.h"
#include <QFile>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void newTab();
    void open();
    void save();
    bool saveAs();
    void changeCurrentTab();

private:
    Ui::MainWindow *ui;
    QMenu *mFileMenu;
    QAction *mNewTabAction;
    QAction *mOpenAction;
    QAction *mSaveAction;
    QAction *mSaveAsAction;
    TabTextEdit *mCurrentTab;

    int mTabsNr = -1;

    void addMenuItems();
    void createTab(const QString &tabName = "", const QString &tabContent = "", const QString &filePath = "");
    bool writeToFile(QFile *filename);
};

#endif // MAINWINDOW_H

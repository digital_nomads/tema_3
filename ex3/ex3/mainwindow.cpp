#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tabtextedit.h"

#include <QMessageBox.h>
#include <QFileDialog.h>
#include <QDir.h>
#include <QFile>
#include <QTextStream>

/*
* Constructor for the main window, removes the normal tab from the designer and adds custom QTextEdit tab
*/
MainWindow::MainWindow(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::MainWindow)
{
   ui->setupUi(this);
   addMenuItems();
   ui->tabWidget->removeTab(0);
   createTab();
}

/*
* Connects signals to slots 
*/
void MainWindow::addMenuItems()
{
   mFileMenu = ui->menuBar->addMenu(tr("File"));

   mNewTabAction = new QAction("New");
   mOpenAction = new QAction("Open");
   mSaveAction = new QAction("Save");
   mSaveAsAction = new QAction("Save as");

   connect(mNewTabAction, &QAction::triggered,         this, &MainWindow::newTab);
   connect(mOpenAction,   &QAction::triggered,         this, &MainWindow::open);
   connect(mSaveAction,   &QAction::triggered,         this, &MainWindow::save);
   connect(mSaveAsAction, &QAction::triggered,         this, &MainWindow::saveAs);
   connect(ui->tabWidget, &QTabWidget::currentChanged, this, &MainWindow::changeCurrentTab);

   mFileMenu->addAction(mNewTabAction);
   mFileMenu->addAction(mOpenAction);
   mFileMenu->addAction(mSaveAction);
   mFileMenu->addAction(mSaveAsAction);
}

/*
* Destructor for all actions
*/
MainWindow::~MainWindow()
{
   delete mCurrentTab;
   delete ui;
   delete mFileMenu;
   delete mNewTabAction;
   delete mOpenAction;
   delete mSaveAction;
   delete mSaveAsAction;
}
/*
* Adds a new tab, uses helper function createTab()
*/
void MainWindow::newTab()
{
   createTab();
}
/*
* Creates a new tab, depending if its new or from an existing file
*/
void MainWindow::createTab(const QString &tabName, const QString &tabContent, const QString &filePath)
{

   TabTextEdit *newTab = new TabTextEdit();
   mTabsNr++;
   if (tabName == "") {
      ui->tabWidget->addTab(newTab, "new " + QString::number(mTabsNr));
   } else {
      ui->tabWidget->addTab(newTab, tabName);
      newTab->setSaved(true);
      newTab->setPath(filePath);
      newTab->setName(tabName);
   }

   newTab->setText(tabContent);
   ui->tabWidget->setCurrentIndex(mTabsNr);
}

/*
* Opens a text file in a new tab
*/
void MainWindow::open()
{
   QString filePath = QFileDialog::getOpenFileName(this, tr("Open"), QDir::homePath(), "Text file(*.txt)");
   QFile file(filePath);

   if (!file.open(QFile::ReadOnly | QFile::Text)) {
      QMessageBox::warning(this, tr("filename"), "Didn't open.");
   } else {
      QTextStream in(&file);
      QString text = in.readAll();
      QFileInfo fileInfo(file.fileName());
      QString filename(fileInfo.fileName());
      createTab(filename, text, filePath);
   }

   file.close();
}

/*
* Saves the content from the tab, if its a new tab it calls the SaveAs() function
*/
void MainWindow::save()
{

   if (!mCurrentTab->isSaved()) {
      if (saveAs()) {
         mCurrentTab->setSaved(true);
         ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), mCurrentTab->getName());
      }
   } else {
      QFile file(mCurrentTab->getPath());
      writeToFile(&file);
      ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), mCurrentTab->getName());
   }
}

/*
* Opens a dialog screen and saves the tab at the selected path
* Returns true if the operation was succesful false otherwise
*/
bool MainWindow::saveAs()
{

   QString filePath = QFileDialog::getSaveFileName(this, tr("Save as"), QDir::homePath(), "Text file(*.txt)");
   QFile file(filePath);

   if (writeToFile(&file)) {
      QFileInfo fileInfo(file.fileName());
      QString filename(fileInfo.fileName());
      mCurrentTab->setName(filename);
      mCurrentTab->setPath(filename);
      return true;
   }
   return false;
}

/*
* Function that keeps track of the current selected tab
*/
void MainWindow::changeCurrentTab()
{
   mCurrentTab = dynamic_cast<TabTextEdit *>(ui->tabWidget->widget(ui->tabWidget->currentIndex()));
}

/*
* Helper function used to save the content of the tab to a file
*/
bool MainWindow::writeToFile(QFile *file)
{

   if (!file->open(QFile::WriteOnly | QFile::Text)) {
      QMessageBox::warning(this, tr("filename"), "Didn't save.");
      file->flush();
      file->close();
      return false;
   }
   QTextStream out(file);
   QString text = mCurrentTab->toPlainText();
   out << text;
   file->flush();
   file->close();
   return true;
}

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->centralWidget()->setAutoFillBackground(true);
    connect(ui->redSlider, SIGNAL(valueChanged(int)), this, SLOT(changeBackgroundColor()));
    connect(ui->greenSlider, SIGNAL(valueChanged(int)), this, SLOT(changeBackgroundColor()));
    connect(ui->blueSlider, SIGNAL(valueChanged(int)), this, SLOT(changeBackgroundColor()));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::changeBackgroundColor()
{
    QColor bColor(ui->redSlider->value(), ui->greenSlider->value(), ui->blueSlider->value());
    QPalette cPallete(bColor);
    this->centralWidget()->setPalette(cPallete);
}

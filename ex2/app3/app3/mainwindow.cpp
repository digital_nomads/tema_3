#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<qlineedit.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(pressed()), this, SLOT(addLineEdit()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addLineEdit()
{
    QLineEdit *lineEdit = new QLineEdit();
    ui->vLayout->addWidget(lineEdit);
}

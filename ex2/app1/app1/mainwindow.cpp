#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(pressed()), this, SLOT(copyText()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::copyText()
{
    ui->lineEditDestination->setText(ui->lineEditSource->text());
    //ui->lineEditDestination->setText(ui->lineEditDestination->text() + ui->lineEditSource->text());   //if we want to append the text.
}
